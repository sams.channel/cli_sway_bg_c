/**
  * CLI application to set background/wallpaper on a system using swaywm.
  * Author: Sam St-Pettersen, 2023
  * Released under the MIT License
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX (100)
#define BUFFER_SIZE (100)

// Forward declarations for display methods.
int displayError(char*, char*);
int displayUsage(char*, int);

int setBackground(char* program, char* bg, char* cfg) {
    // Check that background file exists,
    // if not throw an error.
    if (access(bg, F_OK) == -1) {
        char err[MAX];
        sprintf(err, "Background file does not exist: \"%s\"", bg);
        return displayError(program, err);
    }

    char* temp = "sway.tmp";
    FILE* ifile = fopen(cfg, "r");
    FILE* ofile = fopen(temp, "w");

    char* substr = "output * bg";
    char bgline[MAX];
    sprintf(bgline, "output * bg \"%s\" fill\n", bg);

    char buffer[BUFFER_SIZE];
    while (fgets(buffer, BUFFER_SIZE, ifile) != NULL) {
        if (strstr(buffer, substr) != NULL) {
            fputs(bgline, ofile);
        }
        else {
            fputs(buffer, ofile);
        }
    }

    fclose(ifile);
    fclose(ofile);

    remove(cfg);
    rename(temp, cfg);

    printf("Set background to:\n\"%s\".\n", bg);
    printf("Press Mod+Shift+c to refresh swaywm settings.\n");

    return 0;
}

int displayError(char* program, char* err) {
    printf("Error: %s.\n\n", err);
    return displayUsage(program, -1);
}

int displayUsage(char* program, int exitCode) {
    printf("CLI utility to set background/wallpaper on a system using swaywm.\n");
    printf("Written by Sam St-Pettersen <s.stpettersen@pm.me>\n\n");
    printf("Usage: %s OPTION\n\n", program);
    printf("Options:\n");
    printf("bg=PATH_TO_BG_IMAGE     Specify the background/wallpaper to set.\n");
    return exitCode;
}

int main(int argc, char* argv[]) {
    int exitCode = 0;
    char* program = "cli_sway_bg";

    #ifdef _WIN32
        printf("This program is only intended for Unix-like OSes.\n");
        return exitCode;
    #endif

    char* user = getenv("USER");
    char cfg[MAX];
    sprintf(cfg, "/home/%s/.config/sway/config", user);

    if (access(cfg, F_OK) == -1) {
        printf("Error: swaywm not detected.\n");
        printf("Please install it and create your configuration\n");
        printf("under ~/.config/sway/\n");
        exitCode = displayUsage(program, -1);
        return exitCode;
    }

    // Create a symbolic link (~./swaycfg) to swaywm main
    // configuration file if it does not already exist.
    char link[MAX];
    sprintf(link, "/home/%s/swaycfg", user);

    #ifdef __unix__
        if (access(link, F_OK) == -1) {
            if (symlink(cfg, link) != 0) {
                printf("Error creating symlink.\n");
            }
            else {
                printf("Created a symbolic link to swawywm main config file:\n");
                printf("~/swaycfg\n");
            }
        }
    #endif

    char substr[MAX] = "bg=";
    if (argc > 1) {
        int i;
        for (i = 0; i < argc; i++) {
            if (strstr(argv[i], substr) != NULL) {
                char *ptr, a[MAX];
                ptr = strstr(argv[i], substr);
                if (ptr != NULL) {
                    strcpy(a, ptr + strlen(substr));
                    *ptr = '\0';
                    strcat(argv[i], "");
                    strcat(argv[i], a);
                    exitCode = setBackground(program, a, cfg);
                    return exitCode;
                }
            }
            else if (strstr(argv[i], program) == NULL && i > 0) {
                char err[MAX];
                sprintf(err, "\"%s\" is not a valid option", argv[i]);
                exitCode = displayError(program, err);
                return exitCode;
            }
        }
    }
    else {
        exitCode = displayUsage(program, 0);
    }

    return exitCode;
}
