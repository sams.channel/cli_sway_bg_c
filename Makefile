TARGET=cli_sway_bg
SOURCE=cli_sway_bg.c
COMPILER=gcc
ALT_COMPILER=tcc

make:
	$(COMPILER) $(SOURCE) -o $(TARGET)
	strip $(TARGET)

tcc:
	$(ALT_COMPILER) $(SOURCE) -o $(TARGET)

install:
	@echo Please run this as sudo/doas.
	upx -9 -o /usr/bin/$(TARGET) $(TARGET)

uninstall:
	@echo Please run this as sudo/doas.
	rm /usr/bin/$(TARGET)

clean:
	rm $(TARGET)
